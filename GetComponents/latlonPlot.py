#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function

import numpy as np
import geofunc.geo as geo
import datetime
import pandas as pd
from datetime import datetime
from datetime import timedelta
from datetime import datetime as dt

from matplotlib import cm
import matplotlib.pyplot as plt
import matplotlib

matplotlib.pyplot.stem
matplotlib.axes.Axes.stem

from configparser import ConfigParser

file = r'/home/sindrim/anaconda3/Verkefni/multiplots/configFolder/config.ini'
config = ConfigParser()
config.read('config.ini')



#######################################################
#####       Funtions for latlon plots         #########
#######################################################

####   Scatter Longitude earthquakes
def plotLon(Q2, Y2, seis_maxes, constr, seis_axis, dt_start2, pend2, sid):
   
    min_lon = float(config.get(sid,'min_lon'))
    max_lon = float(config.get(sid,'max_lon'))
    #Set axis limit
    seis_maxes[0].set_zorder(0) 
    seis_maxes[0].set_ylim(min_lon, max_lon)
    seis_maxes[1].set_yticks([])
    
    seis_maxes[0].set_xlim([dt_start2, pend2])
    seis_maxes[0].set_ylabel("Longitude", fontsize=20, labelpad=4)
    #seis_maxes[0].set_ylim(-19.3,-17)
    
    seis_maxes[0].scatter(Y2.index, Y2["longitude"], alpha = 0.75, c=Y2['Mcolors'], label='Cumulative moment', zorder=4, s=4*3.5**Y2['MLW'], marker = 'o',edgecolors='face')
    
    seis_maxes[0].scatter(Q2.index, Q2["longitude"], alpha = 0.75, c=Q2['Mcolors'], label='Cumulative moment', zorder=4, s=4*3.5**Q2['MLW'], marker = 'o',edgecolors='black')
    
    area = [ "min_lon",  "max_lon", "min_lat",  "max_lat"]
    seis_infotext="Seismicity in the area\n {:.2f} to {:.2f} and {:.2f} to {:.2f}".format(*[ constr[key] for key in  area ] ) 
    seis_axis.text(0.50, 0.90, seis_infotext, fontsize=15, transform=seis_axis.transAxes,
                       bbox=dict(facecolor='orange', alpha=0.1), ha='center'  )

    return
    


####   Scatter Latitude earthquakes...
def plotLat(Q2, Y2, seis_maxesLat, dt_start2, pend2, sid):
    
    min_lat = float(config.get(sid,'min_lat'))
    max_lat = float(config.get(sid,'max_lat'))
   
    seis_maxesLat[0].set_zorder(0)
    seis_maxesLat[1].set_zorder(1)
    
    #y-axis not always showing up correctly
    seis_maxesLat[0].set_ylim(min_lat, max_lat)
    
    seis_maxesLat[0].set_xlim([dt_start2, pend2])
    seis_maxesLat[1].set_yticks([])
    seis_maxesLat[0].set_ylabel("Latitude", fontsize=20, labelpad=4)
    #Scatter Latitude earthquakes
    seis_maxesLat[0].scatter(Y2.index, Y2["latitude"], alpha = 0.75, c=Y2['Mcolors'], zorder=4, s=4*3.5**Y2['MLW'], marker = 'o',edgecolors='face')
    
    seis_maxesLat[0].scatter(Q2.index, Q2["latitude"], alpha = 0.75, c=Q2['Mcolors'], zorder=4, s=4*3.5**Q2['MLW'], marker = 'o',edgecolors='black')

    return



def plotDepth(Q2,Y2, seis_maxesDepth, dt_start2, pend2):
    
    seis_maxesDepth[0].set_xlim([dt_start2, pend2])
    seis_maxesDepth[0].set_zorder(1)
    seis_maxesDepth[0].patch.set_visible(False)
    
    seis_maxesDepth[1].scatter(Y2.index, Y2["depth"], alpha = 0.75, c=Y2['Mcolors'], label='Cumulative moment', zorder=4, s=4*3.5**Y2['MLW'], marker = 'o',edgecolors='face')
    
    seis_maxesDepth[1].scatter(Q2.index, Q2["depth"], alpha = 0.75, c=Q2['Mcolors'], label='Cumulative moment', zorder=4, s=4*3.5**Q2['MLW'], marker = 'o',edgecolors='black')
    
    seis_maxesDepth[0].set_ylim(20, 0)
    seis_maxesDepth[1].set_yticks([])

    return



def plotGps(GPS, gps_maxes,gps_axis, start, pend, sid):
    ###Make the moment axis dynamic
    max_value = GPS.up.max()
    if GPS.hlength.max() > max_value:
        max_value = GPS.hlength.max()
    
    min_value = GPS.up.min()
    if GPS.hlength.min() < min_value:
        min_value =  GPS.hlength.min()
        
    set_min = min_value - 30
    if min_value > 30:
        set_min = -30
    
    
    max_value = max_value+30

    gps_maxes[0].set_ylim(ymin =set_min, ymax = max_value)
    gps_maxes[0].set_zorder(1)
    gps_maxes[0].patch.set_visible(False)
    gps_maxes[0].set_xlim([start, pend])
    gps_maxes[0].errorbar(GPS.index, GPS.hlength, yerr=GPS.Dhlength, ls='none', ecolor='grey', elinewidth=0.2, 
                     capsize=0.6, capthick=0.2)
    gps_maxes[0].set_ylabel("Displacement\n[mm]", fontsize=20, labelpad=4)
    gps_maxes[0].plot_date(GPS.index, GPS.hlength, marker='o', markersize=8, markerfacecolor='r', 
               markeredgecolor='r', label='Horizontal displacemt')

    gps_maxes[0].errorbar(GPS.index, GPS.up, yerr=GPS.Dup, ls='none', ecolor='grey', elinewidth=0.2, 
                         capsize=0.6, capthick=0.2)
    #gps_maxes[1].set_ylabel("Vertical\ndisplacement [mm]", fontsize='large', labelpad=4)
    gps_maxes[0].plot_date(GPS.index, GPS.up, marker='o', markersize=8, markerfacecolor='black', 
               markeredgecolor='black', label='Vertical displacemt')
    gps_maxes[0].legend(loc=(0.05,0.67), numpoints=1, frameon=True, edgecolor="white", markerscale=2, framealpha=0.5)

    gps_infotext="GPS station {}".format(sid)
    gps_axis.text(0.5, 0.9, gps_infotext, fontsize=15, transform=gps_axis.transAxes, 
                  bbox=dict(facecolor='orange', alpha=0.1), ha='center' )
    
    return



#this plot is drawn on top of the gps plot:    plotGps(GPS,gps_maxes,gps_axis, start, pend, sid):
def plotMomentAndcolorCodeQukes(Seis, start, pend, gps_maxes):
    
    # this line needs to be improved, causes anomaly/abnormal line in the moment plot, can be pretty ridiculous 
    # with shorter time periods.
    #Seis['cum_moment'].fillna(method='ffill', inplace=True)
    
    
    #Better fix alternative for the line above. However all rows with empty [nan] values will be dropped, erased.
    Seis.dropna(inplace=True)
    
    xlsc=1.02 # x-axis limits w.r.t. data
    ylsc=1.05 # y-axis limits w.r.t. data (max.abs)
    moment_scale=1e14
    ylim_magnitude=np.array([0, ylsc*max(Seis.MLW)])
    ylim_moment=np.array([0, ylsc*max(Seis.cum_moment)])

    gps_maxes[1].plot(Seis.index, Seis.cum_moment/moment_scale, color="black", zorder=4)
    gps_maxes[1].fill_between(Seis.index, 0, Seis.cum_moment/moment_scale, facecolor=(.8,.8,.8), alpha=0.4)
    
    #fill in the gap between last earthquake and the present time
    x = [Seis.index.max(), dt.now()]
    y = [Seis.cum_moment.max()/moment_scale, Seis.cum_moment.max()/moment_scale]
    gps_maxes[1].plot(x, y, color="black", label='Cumulative moment', zorder=4)
    gps_maxes[1].fill_between(x, 0, y, facecolor=(.8,.8,.8), alpha=0.4)
    
    cmap = matplotlib.colors.LinearSegmentedColormap.from_list("",["blue","orange","red"])
    
    #create a smaller dataframe for earthquakes bigger tan 4.5
    theBigOnes = Seis.loc[Seis['MLW'] >= 4.5]

    texts = ["Moment magnitude [$\mathrm{M_{W}}$] for reviewed events) / Local magnitude [$\mathrm{M_{L}}$]  for not reviewed events"]
    
    patches = [ plt.plot([],[], marker="o", ms=15,  mec='black', color='gray', 
            label="{:s}".format(texts[i]))[0]  for i in range(len(texts)) ]
   
    gps_maxes[2].legend(facecolor=None, framealpha=0, handles=patches,bbox_to_anchor=(0.215, 0.90), 
           loc='center', ncol=2, numpoints=1 ,fontsize = 20)
    
    gps_maxes[2].patch.set_visible(False)
    gps_maxes[2].set_zorder(5)
    gps_maxes[2].set_xlim([start, pend])
    
    #Plot colorcoded earthquakes by date
    gps_maxes[2].scatter(Seis.index, Seis.MLW, c=Seis.index, cmap=cmap, s = 100,  marker =  "o", linewidth = 0.3, edgecolors='k')
    
    #   Bigger earthquakes are drawn twice to
    #   the lime green stars, inefficient, but they are not many.
    gps_maxes[2].scatter(theBigOnes.index, theBigOnes.MLW, c= 'lime', cmap=cmap, s = 650,  marker =  "*", linewidth = 0.4, edgecolors='k')
    
    gps_maxes[-1].spines['right'].set_position(('axes', 1.05))
    gps_maxes[2].spines['right'].set_position(('axes', 1.05))
    
    gps_maxes[1].set_zorder(3)
    gps_maxes[1].patch.set_visible(False)
    gps_maxes[1].set_xlim([start, pend])
    gps_maxes[1].set_ylabel("Seismic moment\n [$10^{14}\,$Nm]", fontsize=24, labelpad=4)
    
    gps_maxes[3].set_zorder(1)
    gps_maxes[3].set_xlim([start, pend])
    gps_maxes[3].stem(Seis.index,  Seis.MLW, 'gray', markerfmt=" ", use_line_collection = True)
    gps_maxes[3].set_ylabel("Magnitude [$\mathrm{M_{W}}$/$\mathrm{M_{L}}$]", fontsize=24, labelpad=4,)
    
   
    gps_maxes[1].set_ylim(*ylim_moment/moment_scale*1)
    gps_maxes[2].set_ylim(ymin = 0, ymax = 11)
    gps_maxes[3].set_ylim(ymin = 0, ymax = 11)
    
    return
